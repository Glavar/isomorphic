/**
 * Created by igor on 20.02.17.
 */

import React from 'react';
import {render} from 'react-dom';
import {Router, browserHistory} from 'react-router';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';

import routes from './../routes/index.client';
import myStore from './../store';

import DevTools from './../view/containers/DevTools';

const initialState = window.defaultState;
const store = myStore(initialState);

const history = syncHistoryWithStore(browserHistory, store);

import ff from './../style/main.css';

console.log(ff);

render(
    <Provider store={store}>
        <div>
            <Router children={routes} history={history}/>
            {process.env.NODE_ENV !== 'production' && <DevTools />}
        </div>
    </Provider>,
    document.getElementById('app')
);
