/**
 * Created by igorgajduk on 16.12.16.
 */

const gulp = require('gulp');
const babel = require('gulp-babel');
const clean = require('gulp-clean');
const nodemon = require('gulp-nodemon');
const rename = require("gulp-rename");

gulp.task('clean', () => {
    return gulp.src('dist')
        .pipe(clean());
});

gulp.task('build', ['clean'], () => {
    return gulp
        .src(['server/**/*.js'])
        .pipe(babel({
            presets: [
                'react',
                'es2017-node7',
                'stage-0'
            ],
            plugins: ['transform-runtime', "transform-flow-strip-types", "transform-async-to-generator", "transform-decorators-legacy"]
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['build']);

gulp.task('nodemon', () => {

    return nodemon({
        exec: 'node --debug',
        script: './server/server.js',
        watch: 'server/**/*.js',
        env: {NODE_ENV: 'local'},
        ignore: ['.idea/*', 'node_modules/*'],
        verbose: true
    });
});