/**
 * Created by igor on 19.02.17.
 */
'use strict';
require("babel-register")({
    presets: [
        'react',
        'es2017-node7',
        'stage-0'
    ],
    plugins: ['transform-runtime', "transform-flow-strip-types", "transform-async-to-generator", "transform-decorators-legacy"]
});
const server = require('./server/server').default;
const PORT = process.env.PORT || 3000;
server.listen(PORT, '0.0.0.0', function () {
    console.log('Server listening on', PORT);
});