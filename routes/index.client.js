/**
 * Created by igor on 19.02.17.
 */
import React from 'react';
import { App} from './../view/containers';

function errorLoading (err) {
    console.error('Dynamic page loading failed', err);
}

function loadRoute (cb) {
    return (module) => cb(null, module.default);
}

export default {
    component: App,
    childRoutes: [
        {
            path: '/',
            getComponent (location, cb) {
                System.import('Home')
                    .then(loadRoute(cb))
                    .catch(errorLoading);
            }
        },
        {
            path: 'test',
            getComponent (location, cb) {
                System.import('Test')
                    .then(loadRoute(cb))
                    .catch(errorLoading);
            }
        },
        {
            path: '*',
            getComponent (location, cb) {
                System.import('NotFoundRoute')
                    .then(loadRoute(cb))
                    .catch(errorLoading);
            }
        }
    ]
};
