/**
 * Created by igor on 21.02.17.
 */

import Router from 'koa-router';
import controller from './controller';

const router = new Router();

export default (use) => {
    router.get('/api/contact', controller.contactAdd);


    use(router.routes());
    use(router.allowedMethods());
};
