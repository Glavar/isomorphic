/**
 * Created by igor on 20.02.17.
 */
import Koa from 'koa';
import path from 'path';
import cookie from 'koa-cookie';
import staticCache from 'koa-static-cache';
import convert from 'koa-convert';
import session from 'koa-session2';
import body from 'koa-better-body';

import React from 'react';
import Helmet from 'react-helmet';
import {Provider} from 'react-redux';
import {renderToString} from 'react-dom/server';
import {createMemoryHistory, match, RouterContext} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';

import storeCreate from './../store';
import routes from './../routes';
import route from './route';

const app = new Koa();

app.keys = ['some secret hurr'];
app.use(session({
    key: 'session',
    maxAge: 86400000
}, app));
app.use(convert(body()));
app.use(cookie());
app.use(staticCache(path.resolve(__dirname, 'public')));

// uses async arrow functions
app.use(async(ctx, next) => {
    try {
        if (ctx.cookies.get('session') == null) ctx.session.user = {};
        await next(); // next is now a function
    } catch (err) {
        ctx.body = {message: err.message};
        ctx.status = err.status || 500;
    }
});

route(app.use.bind(app));

app.use(async ctx => {
    const store = storeCreate({store: {userAgent: ctx.request.header['user-agent']}});
    const data = store.getState();
    const {url} = ctx.request;
    const memoryHistory = createMemoryHistory(url);
    const history = syncHistoryWithStore(memoryHistory, store);

    match({history, routes, location: url}, (err, redirectLocation, renderProps) => {
        const InitialComponent = (
            <Provider store={store}>
                <RouterContext {...renderProps} />
            </Provider>
        );
        const componentHTML = renderToString(InitialComponent);
        let head = Helmet.rewind();

        const HTML = `
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                ${head.title.toString()}
                ${head.meta.toString()}
                ${head.link.toString()}
                </head>
              <body>
                <div id="app">${componentHTML}</div>
                <script type="application/javascript">
                  window.defaultState = ${JSON.stringify(data)};
                </script>
                <script src="/assets/vendor.bundle.js"></script>
                <script src="/assets/bundle.js"></script>
              </body>
            </html>
        `;
        ctx.body = HTML;
    });
});

export default app;
