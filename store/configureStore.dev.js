/**
 * Created by igor on 05.03.17.
 */
import { createStore, applyMiddleware, compose } from 'redux';
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import rootReducer from './reducers';
import DevTools from './../view/containers/DevTools';

const enhancer = compose(
    applyMiddleware(promise(), thunk, logger()),
    DevTools.instrument()
);

export default function configureStore (initialState) {
    const store = createStore(rootReducer, initialState, enhancer);

    if (module.hot) {
        module.hot.accept('./reducers', () =>
            store.replaceReducer(require('./reducers').default)
        );
    }

    return store;
}
