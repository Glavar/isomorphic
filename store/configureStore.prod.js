/**
 * Created by igor on 05.03.17.
 */
import {createStore, applyMiddleware} from 'redux';
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import rootReducer from './reducers';

// Middleware you want to use in production:
const enhancer = applyMiddleware(promise(), thunk);

export default function configureStore (initialState) {
    // Note: only Redux >= 3.1.0 supports passing enhancer as third argument.
    // See https://github.com/rackt/redux/releases/tag/v3.1.0
    return createStore(rootReducer, initialState, enhancer);
}
