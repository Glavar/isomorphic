/**
 * Created by igor on 19.02.17.
 */

import configStoreDev from './configureStore.dev';
import configStoreProd from './configureStore.prod';

export default process.env.NODE_ENV === 'production' ? configStoreProd : configStoreDev;

