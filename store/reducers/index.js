/**
 * Created by igor on 19.02.17.
 */
import {combineReducers} from "redux";
import {routerReducer} from 'react-router-redux';

import store from "./phoneBook";

export default combineReducers({
    store,
    routing: routerReducer
});
