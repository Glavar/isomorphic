import React from 'react';
import Helmet from 'react-helmet';

const App = (props) => {
    return (
		<div>
			<Helmet title="Haiduk" />
			<div className="container">
				{ props.children }
			</div>
		</div>
    );
};

export default App;
