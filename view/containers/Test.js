import React, {Component} from 'react';
import { Link } from 'react-router';

class Home extends Component {

    render () {
        return (
            <div>
                Test world
                <header>
                    Links:
                    {' '}
                    <Link to="/">Home</Link>
                    {' '}
                    <Link to="/test">Test</Link>
                </header>
            </div>
        );
    }
}

export default Home;
