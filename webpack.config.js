/**
 * Created by igor on 19.02.17.
 */

const path = require('path');
const webpack = require('webpack');

const config = {
    //entry: path.resolve(__dirname, 'client', 'index.js'), // string | object | array
    entry: {
        app: [
            'webpack-dev-server/client?http://0.0.0.0:8080/',
            'webpack/hot/only-dev-server',
            path.resolve(__dirname, 'client', 'index.js')
        ],
        vendor: [
            'react',
            'react-dom',
            path.resolve(__dirname, 'view', 'containers', 'Home.js'),
            path.resolve(__dirname, 'view', 'containers', 'Test.js'),
            path.resolve(__dirname, 'view', 'containers', 'NotFoundRoute.js')
        ]
    },
    // Here the application starts executing
    // and webpack starts bundling

    output: {
        // options related to how webpack emits results

        path: path.resolve(__dirname, 'dist', 'assets'), // string
        // the target directory for all output files
        // must be an absolute path (use the Node.js path module)
        publicPath: "/assets/",

        filename: 'bundle.js', // string
        // the filename template for entry chunks
        chunkFilename: "[id].bundle.js"

    },
    devtool: 'eval',
    resolve: {
        modules: ["node_modules"],
        extensions: ['.js', '.jsx', '.css'],
        descriptionFiles: ["package.json"],
        moduleExtensions: ["-loader"],
        alias: {
            Home: path.resolve(__dirname, 'view', 'containers', 'Home.js'),
            Test: path.resolve(__dirname, 'view', 'containers', 'Test.js'),
            NotFoundRoute: path.resolve(__dirname, 'view', 'containers', 'NotFoundRoute.js'),
        }
    },
    devServer: {
        hot: true,
        proxy: {
            '*': 'http://0.0.0.0:' + (process.env.PORT || 3000)
        },
        host: '0.0.0.0'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                include: path.join(__dirname, 'style'),
                use: [
                    'style-loader',
                    'css-loader?modules&importLoaders=1',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [
                                    require("postcss-import")({ addDependencyTo: webpack }),
                                    require("postcss-url")(),
                                    require('postcss-cssnext')(),
                                    require('postcss-font-magician')(),
                                    require('postcss-utilities')(),
                                    require("postcss-cssnext")(),
                                    require('precss'),
                                    require('autoprefixer')({ browsers: ['>1%']}),
                                    require("postcss-browser-reporter")(),
                                    require("postcss-reporter")()
                                ];
                            }
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                include: path.join(__dirname, 'client'),
                exclude: /node_modules/,
                enforce: 'pre',
                use: [
                    'eslint-loader'
                ]
            },
            {
                test: /\.js$/,
                include: [
                    path.join(__dirname, 'client'),
                    path.join(__dirname, 'routes'),
                    path.join(__dirname, 'store'),
                    path.join(__dirname, 'view')
                ],
                exclude: /node_modules/,
                use: [
                    'react-hot-loader',
                    'jsx-loader',
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: ['transform-async-functions', 'transform-react-display-name', 'transform-decorators-legacy'],
                            cacheDirectory: true
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                include: [
                    path.join(__dirname, 'server'),
                ],
                exclude: /node_modules/,
                use: [
                    'react-hot-loader',
                    'jsx-loader',
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: ['transform-runtime', 'transform-async-functions', 'transform-react-display-name', 'transform-decorators-legacy']
                        }
                    }
                ]
            },
            {
                test: /\.style.js$/,
                loaders: [
                    'style-loader',
                    'css-loader?modules&importLoaders=1',
                    'postcss-loader?parser=postcss-js',
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: ['transform-runtime', 'transform-async-functions', 'transform-react-display-name', 'transform-decorators-legacy']
                        }
                    }
                ]
            },
            {
                test: /\.(gif|png|jpg|jpeg\ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                use: 'file-loader'
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            debug: true,
            options: {
                configuration: {
                    devtool: 'eval'
                },
                eslint: {
                    configFile: path.join(__dirname, '.eslintrc')
                }
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: Infinity,
            filename: 'vendor.bundle.js'
        }),
    ]
};

module.exports = config;
