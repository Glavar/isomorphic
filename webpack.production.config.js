const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin({ filename: 'style.css', disable: false, allChunks: true });

const config = {
    entry: {
        app: path.resolve(__dirname, "client", 'index.js'),
        vendor: [
            'react',
            'react-dom',
            path.resolve(__dirname, 'view', 'containers', 'Home.js'),
            path.resolve(__dirname, 'view', 'containers', 'Test.js'),
            path.resolve(__dirname, 'view', 'containers', 'NotFoundRoute.js')
        ]
    }, // string | object | array
    // Here the application starts executing
    // and webpack starts bundling

    output: {
        // options related to how webpack emits results

        path: path.resolve(__dirname, "dist", "public", "assets"), // string
        // the target directory for all output files
        // must be an absolute path (use the Node.js path module)

        filename: "bundle.js", // string
        // the filename template for entry chunks
        sourceMapFilename: '[name].map',
        chunkFilename: "[id].bundle.js"

    },
    resolve: {
        modules: ["node_modules"],
        extensions: ['.js', '.jsx', '.scss', '.css'],
        descriptionFiles: ["package.json"],
        moduleExtensions: ["-loader"],
        alias: {
            Home: path.resolve(__dirname, 'view', 'containers', 'Home.js'),
            Test: path.resolve(__dirname, 'view', 'containers', 'Test.js'),
            NotFoundRoute: path.resolve(__dirname, 'view', 'containers', 'NotFoundRoute.js'),
        }
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false,
            options: {
                eslint: {
                    configFile: path.join(__dirname, '.eslintrc')
                }
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true,
            },
            output: {
                comments: false
            }
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: Infinity,
            filename: 'vendor.bundle.js'
        }),
        extractCSS
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: extractCSS.extract({
                    fallback: "style-loader",
                    use: [
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        require('postcss-import'),
                                        require('precss'),
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        }]
                })
            },
            {
                test: /\.js$/,
                include: path.join(__dirname, 'client'),
                exclude: /node_modules/,
                enforce: 'pre',
                use: [
                    {
                        loader: 'eslint-loader'
                    }
                ]
            },
            {
                test: /\.js$/,
                include: [
                    path.join(__dirname, 'client'),
                    path.join(__dirname, 'routes'),
                    path.join(__dirname, 'store'),
                    path.join(__dirname, 'view')
                ],
                exclude: /node_modules/,
                use: [
                    'jsx-loader',
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: ['transform-async-functions', 'transform-react-display-name', 'transform-decorators-legacy']
                        }
                    }
                ]
            },
            {
                test: /\.style.js$/,
                loaders: [
                    'style-loader',
                    'css-loader?modules&importLoaders=1',
                    'postcss-loader?parser=postcss-js',
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'es2015', 'stage-0'],
                            plugins: ['transform-async-functions', 'transform-react-display-name', 'transform-decorators-legacy']
                        }
                    }
                ]
            }
        ]
    }
};

module.exports = config;
